package android.example.com.kevin_1202162378_si40int_pab_modul2;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    TextView Isi_saldo, Pilih_waktu2, Pilih_waktu1, Pilih_tanggal2, Pilih_tgl2, Pilih_tgl1, Pilih_wktu1, txtTiket, saldo, Finish;
    Button Topup, BtnBeli;
    String harga, kota;
    int Pilih_tujuan;
    private int Jam, Menit, hari, Bulan, Tahun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        saldo = findViewById(R.id.textView3);

        Intent i = getIntent();
        String in = i.getStringExtra("Done");

        if(in != null) {
            Finish =findViewById(R.id.textView19);
            Finish.setVisibility(View.VISIBLE);
            String Sisa = String.valueOf(i.getIntExtra("Sisa", 0));
            saldo.setText(Sisa);
        }
        Spinner spinner;
        final ArrayAdapter<CharSequence> adapter;

        Topup = findViewById(R.id.inputSaldo);
        Topup.setOnClickListener(new View.OnClickListener() {
            class Topup {
            }

            @Override
            public void onClick(View v) {
                final Dialog Topup_dialog = new Dialog(MainActivity.this);

                Topup_dialog.setContentView(R.layout.topup);
                Topup_dialog.setTitle("ISILAH SALDO ANDA SEKARANG!");

                final EditText isi_saldo = Topup_dialog.findViewById(R.id.isi_saldo);
                Button cancel = Topup_dialog.findViewById(R.id.button3);
                final Button btnTop =Topup_dialog.findViewById(R.id.button4);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Topup_dialog.dismiss();
                    }
                });

                btnTop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                Isi_saldo =findViewById(R.id.textView3);

                        int tambahan = Integer.parseInt(isi_saldo.getText().toString());
                        int saldo_skrg = Integer.parseInt(Isi_saldo.getText().toString());
                        int ttl = saldo_skrg + tambahan;

                        Isi_saldo.setText(String.valueOf(ttl));

                        Topup_dialog.dismiss();
            }
        });
                spinner =findViewById(R.id.spinner);
                adapter = ArrayAdapter.createFromResource(this,R.array.gabungan, android)
                    }
                    }
                });
    }
}
