package android.example.com.kevin_1202162378_si40int_pab_modul2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SummaryActivity extends Activity {
    TextView Tujuan, Berangkat, Pulang, Tgl_plg, Total;
    Button Pembayaran;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        final Intent i = getIntent();
        final Bundle info = i.getExtras();

        final int saldo = info.getInt("saldo");
        Tujuan = findViewById(R.id.textView14);
        Berangkat = findViewById(R.id.textView15);
        Pulang = findViewById(R.id.textView17);
        Tgl_plg = findViewById(R.id.textView18);
        Total = findViewById(R.id.textView20);

        if (info.getInt("switch") == 1) {
            Pulang.setVisibility(View.VISIBLE);
            Tgl_plg.setVisibility(View.VISIBLE);
        }
        String BerangkatTxt = info.getString("Tgl_berangkat") + " - " + info.get("Jam_berangkat");
        String PulangTxt = info.getString("Tgl_Pulang")+" - " + info.getString("Jam_pulang");

        Tujuan.setText(info.getString("Tujuan"));
        Berangkat.setText(BerangkatTxt);
        Pulang.setText(PulangTxt);
        Total.setText(info.getString("Total"));

        Pembayaran = findViewById(R.id.button2);
        Pembayaran.setOnClickListener ((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Total_biaya = Integer.parseInt(Total.getText().toString());
                int Sisa_saldo = saldo - Total_biaya;

                Intent intent = new Intent (SummaryActivity.this, MainActivity.class);
                intent.putExtra("Done", "Yes");
                intent.putExtra("Sisa", Sisa_saldo);
                SummaryActivity.this.startActivity(intent);
            }
        }));
    }
}
